import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';
import { View, Text } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Platform } from 'react-native';
import React from 'react';
import Home from '@Components/Home';

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home
  }
},{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#FD0011',
    },
    title:'Singapore, Singapore',
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
})

export default createAppContainer(AppNavigator);
