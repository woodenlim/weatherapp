import React, {PureComponent} from 'react';
import AppNavigator from '@Navigators/AppNavigator';
import { Provider } from 'react-redux';
import storeConfig from '@Store/storeConfig';
import {Text, View, SafeAreaView} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
const { store, persistor } = storeConfig();

export default class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <PersistGate /**loading={<Text>Loading</Text>}**/ persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
