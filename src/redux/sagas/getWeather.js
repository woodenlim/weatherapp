import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  getWeatherSuccess,
  getWeatherFail
} from '@ReduxAction/getWeather';
import { Platform } from 'react-native'
import * as actionTypes from '@ReduxAction/actionTypes';
import { axiosPost } from '@Utils/axios'

export function* getWeather() {
  const WEATHER_API = 'https://api.openweathermap.org/data/2.5/forecast?q=Singapore,sg&APPID=fa8344cf5ae289a769f0e456cd72be57&units=metric'
  while (true) {
    const { data } = yield take(actionTypes.GET_WEATHER.REQUEST);
    try {
      const responseData = yield call(axiosPost, WEATHER_API);
      let weatherArray = [];

      responseData.data.list.map((data) => {
        //alert(new Date(data.dt_txt.replace(' ', 'T')).toLocaleTimeString())
        if((Platform.OS === 'ios' ? '5:00:00 AM' : '05:00:00') == new Date(data.dt_txt.replace(' ', 'T')).toLocaleTimeString()){
          weatherArray.push(data)
        }
      })
      //alert('weatherarray:'+weatherArray.length)

      yield put(getWeatherSuccess(weatherArray))
    } catch (error) {
      yield put(getWeatherFail('FAILED'));
    }
  }
}

export default function* root() {
  yield fork(getWeather);
}
