import getWeather from './getWeather';

import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([
    getWeather(),
  ]);
}
