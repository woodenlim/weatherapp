const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILED = 'FAILED';
const CREATE = 'CREATE';
const READ = 'READ';
const UPDATE = 'UPDATE';
const DELETE = 'DELETE';

function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILED, CREATE, READ, UPDATE, DELETE].forEach(type => res[type] = `${base}_${type}`);
  return res;
}
export const GET_WEATHER = createRequestTypes('GET_WEATHER'); 
