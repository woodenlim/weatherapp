import * as actionTypes from '@ReduxAction/actionTypes';

export function getWeatherRequest(data) {
  return {
    type: actionTypes.GET_WEATHER.REQUEST,
    data
  }
}

export function getWeatherSuccess(response) {
  return {
    type: actionTypes.GET_WEATHER.SUCCESS,
    response
  }
}

export function getWeatherFail(error) {
  return {
    type: actionTypes.GET_WEATHER.FAILED,
    error
  }
}
