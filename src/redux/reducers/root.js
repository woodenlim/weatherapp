import {
    combineReducers
} from 'redux'

import getWeather from './getWeather';

const appReducer = combineReducers({
  getWeather
});

export default rootReducer = (state, action) => {
  return appReducer(state, action)
}
