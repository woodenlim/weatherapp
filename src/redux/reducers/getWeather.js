import * as actionTypes from '@ReduxAction/actionTypes';

const initialState =  {
  message: "",
  status: "",
  response: {}
};

export default getWeather = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_WEATHER.REQUEST:
      return {
        ...state
      }
    case actionTypes.GET_WEATHER.SUCCESS:
      return {
        ...state,
        message: 'Get Weather Success',
        status: 'Success',
        response: action.response
      }
    case actionTypes.GET_WEATHER.FAILED:
      return {
        ...state,
        message: 'Get Weather Failed',
        status: 'Failed',
        response: {}
      }
    default:
      return state;
  }
}
