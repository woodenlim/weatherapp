import axios from 'axios';

export const  axiosPost = async (apiUrl) => {
  let result = await axios.post(apiUrl)
  return result
}
