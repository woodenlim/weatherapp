import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator
} from "react-native";
import { SafeAreaView } from 'react-navigation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import { getDayName, getMonthName, getDateInTwoDigit } from '@Utils/dateUtils';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as getWeatherAction from "@ReduxAction/getWeather";

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      response: []
    }
  }

  componentDidMount() {
    this.props.actions.getWeatherRequest();
  }

  componentDidUpdate(prevProps){
    if(prevProps.getWeather !== this.props.getWeather){
      console.log('Before Success',this.props.getWeather.response);
      if(this.props.getWeather.status == 'Success'){
        //alert(this.props.getWeather.response.length)
        console.log('Success',this.props.getWeather);
        this.setState({
          loading: false,
          response: this.props.getWeather.response
        })
      }
    }
  }

  weatherLoading = () => {
    return this.state.loading && <ActivityIndicator size="large" color="black" />
  }

  keyExtractor = (item, index) => item + index;

  renderItem = ({item}) => {
    return (
      <View style={{flex:1, flexDirection:'row', borderBottomWidth:1, borderColor: 'grey', paddingBottom: hp(3)}}>
        <View style={{flex:1, justifyContent:'center', paddingTop: hp(3), paddingLeft:wp(6), paddingRight:wp(6)}}>
          <Text style={{fontSize: hp(2), fontWeight:'600'}}>{this.formatItemDate(item.dt_txt)}</Text>
          <Text style={{fontSize: hp(2)}}>{Math.ceil(item.main.temp_min)} - {Math.ceil(item.main.temp_max)}</Text>
          <Text style={{fontSize: hp(2), color: 'grey',}}>{item.weather[0].main}</Text>
        </View>
        <View style={{flex:0.3, justifyContent:'center'}}>
          <FontAwesome5 name={'chevron-right'} color="#FD0011" size={hp(2)} style={{alignSelf:'center'}}/>
        </View>
      </View>
    )
  }

  formatDate = (date) => {
    //alert(new Date(date.replace(' ', 'T')))
    let day = getDayName('short', new Date(date.replace(' ', 'T')).getDay())
    let month = getMonthName('short', new Date(date.replace(' ', 'T')).getMonth())
    let formattedDate = getDateInTwoDigit(new Date(date.replace(' ', 'T')).getDate())
    let year = new Date(date.replace(' ', 'T')).getFullYear()
    let hours = new Date(date.replace(' ', 'T')).getHours()
    let minutes = new Date(date.replace(' ', 'T')).getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;

    return day + ', ' + formattedDate + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes + ' ' + ampm + ' SGT'
  }

  formatItemDate = (date) => {
    let day = getDayName('short', new Date(date.replace(' ', 'T')).getDay())
    let month = getMonthName('short', new Date(date.replace(' ', 'T')).getMonth())
    let formattedDate = getDateInTwoDigit(new Date(date.replace(' ', 'T')).getDate())
    let year = new Date(date.replace(' ', 'T')).getFullYear()
    let hours = new Date(date.replace(' ', 'T')).getHours()
    let minutes = new Date(date.replace(' ', 'T')).getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;

    return formattedDate + ' ' + month + ' ' + year + ', ' + day
  }

  render() {
    const { response } = this.state
    //alert(response[0].dt_txt)
    return (
      <SafeAreaView style={{flex:1}}>
        {this.weatherLoading()}
        <View>
          <View style={{alignItems:'center', justifyContent:'center', paddingTop: hp(3)}}>
            <Text style={{fontSize: hp(2.5), fontWeight:'600', marginBottom:hp(1)}}>{response.length > 0 && this.formatDate(response[0].dt_txt)}</Text>
            <Text style={{fontSize: hp(5), letterSpacing: 2}}>{response.length > 0 && Math.ceil(response[0].main.temp)}</Text>
            <Text style={{fontSize: hp(3), color: 'grey',}}>{response.length > 0 && response[0].weather[0].main}</Text>
          </View>
          <FlatList
            data={response}
            extraData={response}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />
        </View>
      </SafeAreaView>
    );
  }
}

mapStateToProps = (state) => {
  const { getWeather } = state;
  return {
    getWeather
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},getWeatherAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
